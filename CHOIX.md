## Mes choix pour Share Code :
voir : https://framagit.org/jpython/share-code 

- Flask
  - gabarit html jinja2 (même langage que Django)

- Fichiers pour le stockage puis mariadb

- le minimum de js (c'est mon choix)
- le minimum de css (c'est mon choix)

- call "classique" (renvoient du html)
  ET une api JSON

- page d'admin

- déploiement sur AWS EC2 avec Apache ou Nginx
  ou Docker...
